#!/usr/bin/env python3

# File: evaluate_score.py 
# Author(s): Saswati Ray
# Created: Wed Feb 17 06:44:20 EST 2021 
# Description:
# Acknowledgements:
# Copyright (c) 2021 Carnegie Mellon University
# This code is subject to the license terms contained in the code repo.

import pandas as pd
import sys
from sklearn import metrics
import math

target = sys.argv[3]
metric = sys.argv[4]
Ytest = pd.read_csv(sys.argv[1])[target]
predictions = pd.read_csv(sys.argv[2])[target]

if metric == 'MSE' or metric == 'meanSquaredError':
    print(metrics.mean_squared_error(Ytest, predictions))
elif metric == 'F1Macro' or metric == 'f1Macro':
    print(metrics.f1_score(Ytest, predictions, average='macro'))
elif metric == 'F1' or metric == 'f1':
    pos_label=sys.argv[5]
    if pos_label == '1':
        pos_label = int(pos_label)
    print(metrics.f1_score(Ytest, predictions, pos_label=pos_label))
elif metric == 'MAE' or metric == 'meanAbsoluteError':
    print(metrics.mean_absolute_error(Ytest, predictions))
elif metric == 'ACC' or metric == 'accuracy':
    print(metrics.accuracy_score(Ytest, predictions))
elif metric == 'NMI' or metric == 'normalizedMutualInformation':
    print(metrics.normalized_mutual_info_score(Ytest, predictions))
elif metric == 'RMSE' or metric == 'rootMeanSquaredError':
    print(math.sqrt(metrics.mean_squared_error(Ytest, predictions)))
elif metric == 'ROCAUC':
    predictions = pd.read_csv(sys.argv[2])
    test_preds = pd.DataFrame(columns=['p0','p1']).astype('int64')
    for index, row in predictions.iterrows():
        d3mIndex = str(int(row['d3mIndex']))
        test_preds.at[d3mIndex, 'p' + str(int(row[str(target)]))] = row['confidence']
    #calculate auc:
    test_auc_score = metrics.roc_auc_score(Ytest, test_preds['p1'])
    print(test_auc_score)
elif metric == 'R2' or metric == 'rSquared':
    print(metrics.r2_score(Ytest, predictions))
